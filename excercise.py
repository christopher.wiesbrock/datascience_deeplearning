# Setup plotting
import matplotlib.pyplot as plt
import pandas as pd

plt.style.use('seaborn-whitegrid')
# Set Matplotlib defaults
plt.rc('figure', autolayout=True)
plt.rc('axes', labelweight='bold', labelsize='large',
       titleweight='bold', titlesize=18, titlepad=10)

path=r'C:\Users\wiesbrock\Desktop\winequality-red.csv'
df=pd.read_csv(path, delimiter=';')

df.shape # (rows, columns)

input_shape = [11]

from tensorflow import keras
from tensorflow.keras import layers
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import make_column_transformer, make_column_selector
from sklearn.model_selection import train_test_split
import numpy as np
import seaborn as sns



model = keras.Sequential([
    layers.Dense(8, input_shape=[11]),
    layers.Activation('relu'),
    layers.Dense(8),
    layers.Activation('relu'),
    layers.Dense(1),
])

model.compile(
    optimizer='adam',
    loss='mae',
    metrics=["accuracy"]
)

preprocessor = make_column_transformer(
    (StandardScaler(),
     make_column_selector(dtype_include=np.number)),
    (OneHotEncoder(sparse=False),
     make_column_selector(dtype_include=object)),
)

y=df['quality']
X=df. loc[:, df. columns != 'quality']
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

X = preprocessor.fit_transform(X_train)
y = np.log(y_train) # log transform target instead of standardizing

history = model.fit(X_train, y_train, epochs=10, batch_size=32, validation_data=(X_val, y_val))
# Start the plot at epoch 5. You can change this to get a different view.

test_set_X=df. loc[:, df. columns != 'quality']
test_set_X=test_set_X[::2]
test_y=df['quality']
test_y=test_y[::2]
y_pred=model.predict(test_set_X)
y_pred=np.reshape(y_pred,(len(y_pred)))
y_pred=np.round(y_pred,0)



diff=y_pred-test_y
plt.figure()
sns.histplot(diff)
print(np.std(diff))









